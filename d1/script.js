console.log("hello");

/*
Custom Objects
*/

/*
What is OOP?

A way of programmin applications where reusable plueprints called classes are used to create objects.

Benefits:
- Minimizes repetition
- improves scalability
- improves maintainability
- improves security

Object creation is at the heart of OOP.
*/

/*
Object Literals {}

We have seen that JS comes with predefined objects. We can also create objects of our own through the use of object literals.
*/

/*======================================================*/


// Use an object literal: {} to create an object representing a user
/*
	Encapsulation
	whenever we add properties or methods to an object, we are prforming ENCAPSULATION. The organization of information (as properties) and behavior (as method) to belong to the object that encapsulates then

	The scope of encapsulation is denoted by objects literals
*/

let studentOne = {
	'name': 'John',
	'email': 'john@mail.com',
	'grades': [89, 94, 78, 88],

	//methods
	//add the functionalities available to a student as object methods
	login(){
		console.log(`${this.email} has logged in`);
	},
	/*Ang porpuse ng 'this', reffer to the object encapsulation the method where "this" is called | within the scope of the object.*/

	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grade}`);
	},


/*Mini Activity*/
	
	//1. Create a function that will get the quarterly average of studentOne's grade
	/*Ans*/
//	computeAve(){
//		let sum = 0;
//		this.grades.forEach(grade => sum = sum + grade);
//		return sum/4;
//	}


	//2. Create a funtion that will return true if average grade is >=85, false otherwise
	/*Ans*/
//	willPass(){
//		return this.computeAve() >= 85 ? true : false;
//	}


	//3. Create a funstion called willPassWithHonors() that returns true if the student has passed and their average is >=90. The function returns false if either one is not met.
	/*Ans*/
//	willPassWithHonors(){
//		return (this.willPass() && this.computeAve() >= 90) ? true : false;
//	}

}

//log the content of studentOne's encapsulated information in the console.
console.log(`student one name is ${studentOne.name}`);
console.log(`student one email is' ${studentOne.email}`);
console.log(`student one quarterly grade average are ${studentOne.email}`);

/*======================================================*/



/*
QUIZ.js(part 2)

1. What is the term given to unorganized code that's very hard to work with?
ans: spaghetti code

2. How are object literals written in JS?
ans: with curly brackets {}

3. What do you call the concept of organizing information and functionality to belong to an object?
ans: OOP Object Oriented Programming

4. If studentOne has a method named enroll(), how would you invoke it?
ans: studentOne.enroll()

5. True or False: Objects can have objects as properties.
ans: true

6. What is the syntax in creating key-value pairs?
ans: let kvp = {'key': 'pair'} 

7. True or False: A method can have no parameters and still work.
ans: true

8. True or False: Arrays can have objects as elements.
ans: true

9. True or False: Arrays are objects.
ans: true

10. True or False: Objects can have arrays as properties.
ans: true

*/





/*part of quiz, function coding*/

//without the use of objects, our students from before would be organized as follows if we are to record additional information about them
//create student one
/*let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}
//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects*/


/*quiz.js(function coding)*/

//1. Translate the other students from our boilerplate code into their own respective objects.
let studentObj1 = {
	'name': 'John',
	'email': 'john@mail.com',
	'grades': [89, 84, 78, 88],

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)
	aveGrade() {
			let sum = 0;
			this.grades.forEach(grade => sum = sum + grade);
			return sum/4
	},

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail.
//For a student to pass, their ave. grade must be greater than or equal to 85.
	willpass() {
		return this.aveGrade() >= 85 ? true : false;
	},

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90,
//false if >= 85 but < 90, and undefined if < 85 (since student will not pass).
	willPassWithHonors() {
		let honors = this.aveGrade();
		if(honors >= 90) {
			return true;
		} else if (honors >= 85){
			return false;
		} else {
			return false;
		}
	},

}


let studentObj2 = {
	'name': 'Joe',
	'email': 'joe@mail.com',
	'grades': [78, 82, 79, 85],
	aveGrade() {
			let sum = 0;
			this.grades.forEach(grade => sum = sum + grade);
			return sum/4
	},
	willpass() {
		return this.aveGrade() >= 85 ? true : false;
	},
	willPassWithHonors() {
		let honors = this.aveGrade();
		if(honors >= 90) {
			return true;
		} else if (honors >= 85){
			return false;
		} else {
			return false;
		}
	},
}


let studentObj3 = {
	'name': 'Jane',
	'email': 'jane@mail.com',
	'grades': [87, 89, 91, 93],
	aveGrade() {
			let sum = 0;
			this.grades.forEach(grade => sum = sum + grade);
			return sum/4
	},
	willpass() {
		return this.aveGrade() >= 85 ? true : false;
	},
	willPassWithHonors() {
		let honors = this.aveGrade();
		if(honors >= 90) {
			return true;
		} else if (honors >= 85){
			return false;
		} else {
			return false;
		}
	},
}


let studentObj4 = {
	'name': 'Jessie',
	'email': 'jessie@mail.com',
	'grades': [91, 89, 92, 93],
	aveGrade() {
			let sum = 0;
			this.grades.forEach(grade => sum = sum + grade);
			return sum/4
	},
	willpass() {
		return this.aveGrade() >= 85 ? true : false;
	},
	willPassWithHonors() {
		let honors = this.aveGrade();
		if(honors >= 90) {
			return true;
		} else if (honors >= 85){
			return false;
		} else {
			return false;
		}
	},
}


//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
let classOf1A = {
	students: [studentObj1, studentObj2, studentObj3, studentObj4],

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.
	countHonorStudents() {
		let honorStudents = this.students.filter(students => students.aveGrade() >= 90);
		return honorStudents.length;
	},

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
	honorsPercentage() {
		let totalStudents = this.students.length;
		let honorStudents = this.countHonorStudents();
		let percentage = (honorStudents / totalStudents) * 100;
		return percentage;
	},

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo()
//that will return all honor students' emails and ave. grades as an array of objects.
	retrieveHonorStudentInfo() {
		let honorStudents = this.students.filter(student => student.aveGrade() >= 90);
		let honorStudentInfo = honorStudents.map(student => {
			return {
				email: student.email,
				averageGrade: student.aveGrade()
			};
		});
		return honorStudentInfo;
	},

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc()
//that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
	sortHonorStudentsByGradeDesc() {
		let honorStudents = this.students.filter(student => student.aveGrade() >= 90);
		honorStudents.sort((a, b) => b.aveGrade() - a.aveGrade());
		let sortedHonorStudents = honorStudents.map(student => {
			return {
				email: student.email,
				averageGrade: student.aveGrade()
			};
		});
		return sortedHonorStudents;
	}

};











